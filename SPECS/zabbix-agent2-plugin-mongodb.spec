Name:		zabbix-agent2-plugin-mongodb
Version:	6.0.17
Release:	%{?alphatag:%{?alphatag}.}release1%{?dist}
Summary:	Zabbix Agent2 plugin for monitoring MongoDB installations
Group:		Applications/Internet
License:	Apache 2.0
URL:		http://www.zabbix.com/
Source0:	%{name}-%{version}%{?alphatag}.tar.gz
Patch1:		conf.patch

%description
Zabbix Agent2 plugin for monitoring MongoDB installations

%define debug_package %{nil}

%prep
%setup0 -q -n %{name}-%{version}%{?alphatag}
%patch1 -p1

%build
make

%install
mkdir -p $RPM_BUILD_ROOT%{_sbindir}/zabbix-agent2-plugin/
cp zabbix-agent2-plugin-mongodb $RPM_BUILD_ROOT%{_sbindir}/zabbix-agent2-plugin/
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agent2.d/plugins.d/
cp mongodb.conf $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agent2.d/plugins.d/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE README.md
%{_sbindir}/zabbix-agent2-plugin/zabbix-agent2-plugin-mongodb
%config(noreplace) %{_sysconfdir}/zabbix/zabbix_agent2.d/plugins.d/mongodb.conf

%changelog
* Mon Apr 24 2023 Zabbix Packager <info@zabbix.com> - 6.0.17-release1
- update to 6.0.17

* Wed Feb 1 2023 Zabbix Packager <info@zabbix.com> - 6.0.13-release1
- update to 6.0.13

* Wed Jan 25 2023 Zabbix Packager <info@zabbix.com> - 6.0.13-rc1.release1
- update to 6.0.13rc1

* Thu Nov 17 2022 Zabbix Packager <info@zabbix.com> - 1.2.0-2
- release 2 of 1.2.0

* Mon Oct 31 2022 Zabbix Packager <info@zabbix.com> - 1.2.0-1
- update to 1.2.0

* Tue Sep 27 2022 Zabbix Packager <info@zabbix.com> - 1.1.0-1
- update to 1.1.0

* Mon May 30 2022 Zabbix Packager <info@zabbix.com> - 1.0.0-1
- initial release of mongodb plugin for zabbix agent2
